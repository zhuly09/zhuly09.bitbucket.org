
/**
 * Created by admin on 2017/3/6.
 */

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
gulp.task('sass',function(){
    gulp.src(['./src/*.scss'])
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./assest/css'))
});
gulp.task('default',function(){
    gulp.watch(['./src/*scss'],['sass'])
})